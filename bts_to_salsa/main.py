#!/usr/bin/env python3

"""
.. module:: main
   :synopsis: Debian BTS to Salsa/GitLab issues sync

"""

# Released under AGPLv3+ license, see LICENSE

from argparse import ArgumentParser
from collections import OrderedDict
from time import sleep
import yaml
import debianbts
import logging

from gitlab import Gitlab

log = logging.getLogger(__name__)

DEFAULT_BASEURL = "https://salsa.debian.org"
ABUSE_THROTTLING_TIME = 5


class ParsingError(Exception):
    pass


def setup_logging(debug):
    level = logging.DEBUG if debug else logging.INFO
    log.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)


def parse_args():
    ap = ArgumentParser()
    ap.add_argument('config_filename')
    ap.add_argument('-d', '--debug', action="store_true")
    ap.add_argument('-s', '--dry-run', action="store_true",
                    help="Simulate/dry-run: do not create/update issues")
    return ap.parse_args()


def load_conf(fn):
    """Load configuration"""
    with open(fn) as f:
        conf = yaml.load(f)

    assert 'repositories' in conf
    assert 'gitlab_api_token' in conf
    return conf


def fetch_bug_summary(bug_num):
    bug = debianbts.get_status(bug_num)
    if not bug:
        raise RuntimeError("No BTS data for #%s" % bug_num)

    return bug[0]


def fetch_bug_numbers_by_package(pkg_name):
    """Fetch non-archived bugs"""
    return debianbts.get_bugs('package', pkg_name, 'archive', 'false')


def extract_msg_id(header):
    header = header.splitlines()
    for line in header:
        if line.startswith(('Message-ID:', 'Message-Id')):
            return line[11:].strip()

    log.error("Message-ID not found in comment:")
    for line in header:
        log.error("    %s", line)
    raise ParsingError


def extract_msg_author(header):
    for line in header.splitlines():
        if line.startswith('From:'):
            return line[5:].strip()


def fetch_bug_log(bug_num):
    """Get a bug log (the sequence of comments) from the BTS

    :returns: Message-ID -> (author, body)  dict
    """
    ordered_bug_log = OrderedDict()
    for b in debianbts.get_bug_log(bug_num):
        try:
            msg_id = extract_msg_id(b['header'])
        except ParsingError:
            continue

        author = extract_msg_author(b['header'])
        ordered_bug_log[msg_id] = (author, b['body'])

    return ordered_bug_log


class BugSyncer(object):
    """Sync Bug reports from the Debian BTS to a Salsa/GitLab project
    """
    def __init__(self, conf, dryrun=False):
        self.dryrun = dryrun
        url = conf.get("url", DEFAULT_BASEURL)
        self._glclient = Gitlab(
            url,
            private_token=conf['gitlab_api_token'],
            timeout=30
        )
        sync_label = conf['sync_label']

        for repo_conf in conf['repositories']:
            debian_pkg_name = repo_conf['debian_pkg']
            gitlab_repo_name = repo_conf['gitlab_repo']
            self.sync(debian_pkg_name, gitlab_repo_name, sync_label)

    def fetch_gitlab_issues_by_repo(self, gitlab_repo, sync_label_name):
        """Fetch issues from Salsa/GitLab"""

        g_issues = gitlab_repo.issues.list(
            labels=[sync_label_name],
            as_list=False
        )

        issues = {}
        for issue in g_issues:
            try:
                t = issue.title
                if not len(t) or t[0] != "[":
                    log.error("Unable to parse issue title %r", t)
                    continue

                num = t[1:].split(']', 1)[0]
                num = int(num)
                if num in issues:
                    dup_issue_num = issues[num].number
                    log.error("Duplicate Debian bug %d %d %d", num,
                              dup_issue_num, issue.number)

                issues[num] = issue
            except Exception:
                log.error("Unable to parse issue %r", t, exc_info=True)
                continue

        log.debug("  %d issues currently on Salsa/GitLab", len(issues))
        return issues

    def sync(self, debian_pkg_name, gitlab_repo_name, sync_label_name):
        """Sync bugs from a package to Salsa/GitLab issues in a repository
        """
        log.debug("Mirroring from %s to %s", debian_pkg_name, gitlab_repo_name)

        bug_numbers = fetch_bug_numbers_by_package(debian_pkg_name)
        log.debug("  %d bugs on the BTS", len(bug_numbers))

        gitlab_repo = self._glclient.projects.get(gitlab_repo_name)
        self.throttle()

        # get the special label to flag issues generated from the BTS
        try:
            sync_label = gitlab_repo.labels.get(sync_label_name)
            self.throttle()
        except gitlab.exceptions.GitlabGetError:
            # FIXME
            log.error(
                "Label %r not found: create such bug label on Salsa/GitLab",
                sync_label_name)
            return

        issues = self.fetch_gitlab_issues_by_repo(gitlab_repo, sync_label_name)
        log.debug("  %d issues currently on Salsa/GitLab", len(issues))
        self.throttle()

        for bn in bug_numbers:
            self.sync_bug(bn, debian_pkg_name, issues, gitlab_repo,
                          sync_label_name)

    def sync_bug(self, bn, debian_pkg_name, issues, gitlab_repo,
                 sync_label_name):
        """Sync a bug report
        """
        log.debug("    processing %s: %d", debian_pkg_name, bn)
        summary = fetch_bug_summary(bn)
        self.throttle()

        # Create the Salsa/GitLab issue if needed

        if bn in issues:
            # the issue is already on Salsa/GitLab
            issue = issues[bn]

        elif self.dryrun:
            log.debug("       not creating new issue (dry run)")
            return

        else:
            log.info("       creating new issue")
            issue = gitlab_repo.issues.create({
                'title': "[%d] %s" % (bn, summary.subject),
                'description': "created by BTS sync",
                'labels': [sync_label_name],
            })
            # FIXME self.throttle_abuse_limit()

        bts_bug_logs = fetch_bug_log(bn)
        self.throttle()
        log.debug("      %d comments on the BTS", len(bts_bug_logs))

        # Create issue comments if needed

        for comment in issue.notes.list(as_list=False):
            body = comment.attributes['body'].splitlines()
            for line in body[:5]:
                if line.startswith('BTS_msg_id: '):
                    # This comment on Salsa/GitLab was created from the BTS
                    bts_msg_id = line[11:].strip()
                    bts_bug_logs.popitem(bts_msg_id)
                    break

        log.debug("      %d comments to be created", len(bts_bug_logs))

        for msg_id, comment_data in bts_bug_logs.items():
            # create new comments, hopefully in the correct order
            author, body = comment_data
            newbody = "BTS_msg_id: %s\nBTS author: %s\n\n%s" % \
                (msg_id, author, body)
            if self.dryrun:
                log.debug("    not creating comment (dryrun)")
            else:
                log.info("creating comment")
                issue.notes.create({
                    "body": newbody,
                })
                self.throttle_abuse_limit()

        # Update issue open/close state if needed

        expected_issue_state = u'closed' if summary.done else u'open'
        if issue.state != expected_issue_state:
            # update needed
            if self.dryrun:
                log.debug("    not setting state to %s (dryrun)",
                          expected_issue_state)

            else:
                log.debug("    setting state to %s", expected_issue_state)
                issue.edit(state=expected_issue_state)
                self.throttle_abuse_limit()

    def throttle(self):
        """Throttle API usage by sleeping after every call
        """
        #FIXME
        return
        remaining, total = self._glclient.rate_limiting
        if remaining < 10:
            log.info("Rate limit critical: Sleeping for 1h!")
            sleep(3600)
        else:
            # Exponential backoff
            sleep_time = total * 0.1 / remaining
            sleep(sleep_time)

    def throttle_abuse_limit(self):
        """Throttle API usage to avoid hitting anti-abuse limits
        """
        #FIXME
        return
        sleep(ABUSE_THROTTLING_TIME)


def main():
    args = parse_args()
    setup_logging(args.debug)
    conf = load_conf(args.config_filename)
    BugSyncer(conf, dryrun=args.dry_run)


if __name__ == '__main__':
    main()
