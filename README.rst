
Debian BTS to Salsa/GitLab Issues sync
======================================

Mirrors bugs from the Debian BTS to Salsa / GitLab Issues

`Official repository <https://salsa.debian.org/debian/bts-to-salsa-sync>`_

Usage
-----

Create a API token on GitLab and a label that will be used to tag
the mirrored bugs.

Create a configuration file as::

    ---
    gitlab_api_token: it31might32look74real42but392i24made3it34up
    sync_label: debian-bts

    repositories:
    - debian_pkg: <Debian package name>
      repo: <username>/<project name>



Roadmap
-------

* More safety checks
* Throttling
